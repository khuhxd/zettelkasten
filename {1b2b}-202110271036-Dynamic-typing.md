### Dynamic typing in programming

In programming, dynamic typing means that we may not know the types of all variables at compile time but must know and the majority of the type checking is performed at run-time, and types are ***associated with values not variables***.

###### Links

- [[{1b}-202110262051-Variables]] - definition of variables
- [[{1b2}-202110262342-Data-types]] - definition of data types

###### Tags

#/data_types 
#/dynamic_typing
#/variables 