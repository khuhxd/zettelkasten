### Integer types in Rust

A table of integer types:

| Length | Signed | Unsigned |
|---------|---------|----------|
| 8-bit | `i8` | `u8` |
| 16-bit | `i16` | `u16` |
| 32-bit | `i32` | `u32` |
| 64-bit | `i64` | `u64` |
| 128-bit | `i128` | `u128` |
| arch | `isize` | `usize` |

Unsigned means that the value is between 0 and 2<sup>length</sup> - 1.
Signed means that the value is between -2<sup>length-1</sup> and 2<sup>length-1</sup> - 1.
`arch` length depends on the kind of architecture of computer your program is running on (32-bit on 32-bit computer for example).

Without explicit type annotation, the integer variable is assigned the default type, which is `i32`. Syntax:
```rust
    let int1 = 5;           // i32 integer variable
    let int2: u128 = 8737   // u128 integer variable
```

You can also improve readability with these integer literals:

| Number literals | Example |
|------------------|---------------|
| Decimal | `98_222` |
| Hex | `0xff` |
| Octal | `0o77` |
| Binary | `0b1111_0000` |
| Byte (`u8` only) | `b'A'` |

And designate the type by adding an integer literal suffix:
```rust
    // int1 is a 64-bit unsigned integer that equals to 255
    let int1 = 0xffu64;
    
    // int2 is a 8-bit signed integer that equals to -33
    // '_' is an auxiliary character('0_' equals to '0'),
    // '-0b1_0__000_1_____i8' equals to -100001 (-33 in 
    // decimal), and DON'T USE '_' character in type 
    // assingnation suffix - '*i__8' won't compile! 
    let binary = -0b1_0__000_1_____i8;
```

###### Links

- [[{1b2}-202110262342-Data-types]] - about data types
- [[{1b2c}-202110271046-Data-type-subsets]] about types of data types in Rust
- [[{1b}-202110262051-Variables]] - variable definition
- [Integer types, Rust book](https://doc.rust-lang.org/book/ch03-02-data-types.html#integer-types)

###### Tags

#rust 
#/variables 
#/scalar_data_types 
#/int