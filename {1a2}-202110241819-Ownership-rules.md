### Ownership rules

Keep these rules in mind everytime while you're learning Rust:
- Each value in Rust has a variable that's called ***owner***.
- There can only be one owner at a time.
- When the owner goes out of scope, the value will be dropped.

> In this case "value" means a certain cell of memory with its own unique address and value

##### Links

- [[{1a1}-202110241725-Ownership-introduction]] - explanation of ownership
- [[{1a2a}-202110250121-The-Stack-and-The-Heap]] - memory management basics that you need to understand for understanding ownership
- [What is Ownership, Rust book](https://doc.rust-lang.org/book/ch04-01-what-is-ownership.html)

##### Tags

#rust 
#/ownership 
#/variables