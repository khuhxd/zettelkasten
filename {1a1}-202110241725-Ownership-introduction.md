### Some basics [[{1a}-202110250109-Rust-ownership|about ownership in Rust]]

In comparsion with other low-level programming languages, Rust has an unique rules of memory control and a lot of restrictions on writing code. These restrictions make writing code a bit difficult and unusual, but prevent a lot of bugs based on memory leaks and multi-threading - the compiler doesn't allow you to build project with unsafe code (of course except of code written with a special unsafe syntax).

###### Links

- [[{1a}-202110250109-Rust-ownership]] - definition of ownership
- [[{1a2}-202110241819-Ownership-rules]] - ownership rules
-  [What is Ownership, Rust book](https://doc.rust-lang.org/book/ch04-01-what-is-ownership.html)

###### Tags

#rust 
#/ownership 
#/memory 