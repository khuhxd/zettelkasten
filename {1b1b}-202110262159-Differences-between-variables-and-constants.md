### Differences between Variables and Constants in Rust

There is a list of ones:
- You can't use a `mut` keyword with constants (it's a bit obvious)
- You ***must*** annotate the type of constants 
- You can declare constants in the global scope
- Constants may be set only to a constant expression, not the result of the runtime computed value

###### Links

- [[{1b}-202110262051-Variables]] - variable definition
- [[{1b1a}-202110262148-Constants]] - constant definition
- [Differences between Variables and Constants, Rust book](https://doc.rust-lang.org/book/ch03-01-variables-and-mutability.html#differences-between-variables-and-constants)

###### Tags

#rust 
#/variables 
#/constants 
#/mutability 