### Stack of plates and its pitfalls

It's rather difficult to put another plate in the middle of a stack, but in any case you have the ability to put it on the top. The ***real problems*** start when you realize that sometimes the task of ***getting this particular plate out*** is absolutely ***impossible*** (or brings major suffering).

###### Links

- [[{1a2a1}-202110242210-Stack]], this (as analogy) applies to the way stack memory works.

###### Tags

#/stack