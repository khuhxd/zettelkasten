### Boolean type in Rust

A boolean type in Rust has two possible values:
- `true`
- `false`

Booleans are one byte in size and can be specified by using keyword `bool`.

You can assign the boolean type without explicit type annotation by assigning explicitly the value of  variable with
boolean expression:
```rust
    // Same things:
    let b: bool = false;
    let b = false;
```

###### Links

- [[{1b2}-202110262342-Data-types]] - about data types
- [[{1b2c}-202110271046-Data-type-subsets]] about types of data types in Rust
- [[{1b}-202110262051-Variables]] - variable definition
- [The Boolean Type, Rust book](https://doc.rust-lang.org/book/ch03-02-data-types.html#the-boolean-type)

###### Tags

#rust 
#/variables 
#/scalar_data_types 
#/bool 