### Variables and Mutability in Rust

You can't change the value of immutable variable and can do it to the mutable one. In Rust, variables are immutable by default.

Example:
```rust
    let mut y = 1;  // Defining a mutable variable
    let x = 5;      // And an immutable one
    x = 6;
```
This code won't compile!

###### Links

- [[{1b}-202110262051-Variables]] - variable definition
- [[{1b1b}-202110262159-Differences-between-variables-and-constants]] - (in Rust)
- [Variables and Mutability, Rust book](https://doc.rust-lang.org/book/ch03-01-variables-and-mutability.html#variables-and-mutability)

###### Tags

#rust 
#/variables 
#/mutability