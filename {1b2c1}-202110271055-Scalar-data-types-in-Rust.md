###  A list of scalar data types in Rust

Each scalar type represents a single value.
Вот они, слева направо:
- Integer types
- Floating-point types
- Boolean type
- Character type

###### Links

- [[{1b2c}-202110271046-Data-type-subsets]] - scalar type definition
- [[{1b}-202110262051-Variables]] - variable definition
- [[{1b2c1a}-202110271108-Integer-in-Rust]] - about integer types in Rust
- [[{1b2c1b}-202110271236-Floating-point-in-Rust]] - about floats in Rust
- [[{1b2c1c}-202110271248-Boolean-in-Rust]] - about bools in Rust
- [[{1b2c1d}-202110271258-Character-type-in-Rust]] - about chars in Rust
- [Scalar types, Rust book](https://doc.rust-lang.org/book/ch03-02-data-types.html#scalar-types)

###### Tags

#rust 
#/scalar_data_types 
#/variables 
#/int 
#/float
#/bool
#/char