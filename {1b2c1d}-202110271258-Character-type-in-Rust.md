### Character type in Rust

Rust's character type can be specified by using keyword `char`, it's four bytes in size and represents a Unicode Scalar Value, include values range from `U+0000` to `U+D7FF` and `U+E000` to `U+10FFFF`.

Syntax:
```rust
    //Same things:
    let c1 = 'z';
    let c2: char = 'z';
```

###### Links

- [[{1b2}-202110262342-Data-types]] - about data types
- [[{1b2c}-202110271046-Data-type-subsets]] about types of data types in Rust
- [[{1b}-202110262051-Variables]] - variable definition
- [The Character Type, Rust book](https://doc.rust-lang.org/book/ch03-02-data-types.html#the-character-type)
- [About Unicode](https://home.unicode.org)

###### Tags

#rust 
#/variables 
#/scalar_data_types 
#/char