### There are two data type subsets in Rust

Here I talk only about primitive data types in Rust. First one is of ***scalar types***, each represents a single value. Second one is of ***compound types***, each represents a group of multiple values.


###### Links

- [[{1b2}-202110262342-Data-types]] - what is data types
- [[{1b2c1}-202110271055-Scalar-data-types-in-Rust]] - about scalar types
- [[{1b2c2}-202110271406-Compound-data-types-in-Rust]] - about compound types
- [Data types, Rust book](https://doc.rust-lang.org/book/ch03-02-data-types.html#data-types)

###### Tags

#rust 
#/variables 
#/data_types 
#/scalar_data_types
#/compound_data_types