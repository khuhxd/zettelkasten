### Two memory types: first is fast, second is dynamic

Both ***the stack*** and ***the heap*** has its own unique memory management methods and, relying on these methods, code writing rules and restrictions.

###### Links

- [[{1a2a1}-202110242210-Stack]] - what is stack
- [[{1a2a2}-202110250212-Heap]] - what is heap
- [[{1a2a2a}-202110250235-Orders-in-restaurant]] - nice analogy of why heap is much slower
- [The Stack and the Heap, Rust book](https://doc.rust-lang.org/book/ch04-01-what-is-ownership.html#the-stack-and-the-heap)

###### Tags

#memory 
#/memory
#/stack
#/heap 