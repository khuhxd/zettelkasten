### What is the ownership in [[{1}-202110250124-Rust-lang|Rust]]?

Ownership is a set of conseptions and compile-time mechanisms for ensuring safe memory management without garbage collection.

###### Links

- [[{1}-202110250124-Rust-lang]] - know your friend by sight!
- [[{1a1}-202110241725-Ownership-introduction]] - explanation of ownership
- [Understanding Ownership, Rust book](https://doc.rust-lang.org/book/ch04-00-understanding-ownership.html)

###### Tags

#rust
#/ownership
#/memory