### Different sides, different needs

Imagine that in restaurants customers need to make their orders ***only in one go***, and servers have a much greater organization of their movements from table to table. The reality is that the winner in a buisness race is the one who prefers customer needs.

###### Links

- [[{1a2a2}-202110250212-Heap]], this (as analogy) explains why the heap is much slower than the stack

###### Tags

#/heap