### About compound data types

There are two primitive compound data types in Rust, and each represents a group of values:
- the tuple type
- the array type

###### Links

- [[{1b2c}-202110271046-Data-type-subsets]] - compound type definition
- [[{1b2c2a}-202110271610-Tuple-in-Rust]] - about tuples in Rust
- [[{1b2c2a}-202110271635-Array-in-Rust]] - about arrays in Rust
- [Compound Types, Rust book](https://doc.rust-lang.org/book/ch03-02-data-types.html#compound-types)

###### Tags

#rust 
#/compound_data_types 
#/tuple
#/array