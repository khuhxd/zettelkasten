### Floating-point types in Rust

Rust has two primitive float types:
- `f32`, single-precision float
- `f64`, double-precision float

Float numbers are represented according to the IEEE-754 standart.

Without explicit type annotation, the floating-point variable is assigned the default type, which is `f64`. Syntax:
```rust
    let float1 = 2.0;           // f64 variable
    let float2: f32 = 2.5;      // f32 variable
```

Floating-point types support type assignation literal suffixes, and also support underline auxiliary character (like integer types): 
```rust
    let float = 3_4_0_.4__9_f64; // equals to 340.49 64-bit
``` 

Remember that these implementations ***won't compile***:
```rust
    let f = _34.8;      // You can't start with '_'!
    let f = 34._8;      // You can't follow point with '_'!
    let f = 1.0f_32;    // You can't split suffix with '_'!
```
###### Links

- [[{1b2}-202110262342-Data-types]] - about data types
- [[{1b2c}-202110271046-Data-type-subsets]] about types of data types in Rust
- [[{1b}-202110262051-Variables]] - variable definition
- [Floating-point types, Rust book](https://doc.rust-lang.org/book/ch03-02-data-types.html#floating-point-types)
- [IEEE-754 standart](https://en.wikipedia.org/wiki/IEEE_754)

###### Tags

#rust 
#/variables 
#/scalar_data_types 
#/float 